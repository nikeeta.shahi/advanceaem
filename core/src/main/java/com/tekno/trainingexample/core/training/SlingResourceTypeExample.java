package com.tekno.trainingexample.core.training;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;


@Component(service=Servlet.class,property= {
		
		"sling.servlet.resourceType=teknopoint/components/slingtests",
		"sling.servlet.methods=GET"
})
public class SlingResourceTypeExample extends SlingAllMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5313226687564562624L;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().println("Hello from the sling servlet");
		
		
	}

}
