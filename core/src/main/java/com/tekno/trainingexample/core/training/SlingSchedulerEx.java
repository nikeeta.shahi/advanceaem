package com.tekno.trainingexample.core.training;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service=Runnable.class,property= {
		
		"scheduler.expression=*/30 * * * * ?"
		
},immediate=true)
public class SlingSchedulerEx implements Runnable {
	
	int count = 0;

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void run() {
		logger.info("Scheduler is running {}",++count);

	}

}
