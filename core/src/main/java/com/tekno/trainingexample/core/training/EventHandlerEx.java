package com.tekno.trainingexample.core.training;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service=EventHandler.class,property="event.topics=com/tekno/training/event/topic")
public class EventHandlerEx implements EventHandler {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void handleEvent(Event event) {
		
		logger.info("Event Received with data {}", event.getProperty("resourcepath"));
		

	}

}
