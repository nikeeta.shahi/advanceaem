package com.tekno.trainingexample.core.training;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.UnsupportedRepositoryOperationException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate=true)
public class ObservationEx implements EventListener {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Reference
	ResourceResolverFactory rrf;
	ResourceResolver resResolver;
	ObservationManager observationManager;

	@Activate
	protected void addListener() {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ResourceResolverFactory.SUBSERVICE, "teknoService");
			resResolver = rrf.getServiceResourceResolver(map);
			Session session = resResolver.adaptTo(Session.class);
			observationManager = session.getWorkspace().getObservationManager();
			observationManager.addEventListener(this, Event.PROPERTY_CHANGED | Event.PROPERTY_ADDED,
					"/etc/tekno/projects", true, null, null, true);

		} catch (UnsupportedRepositoryOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Deactivate
	protected void removeLIstener() {
		try {

			if (observationManager != null)
				observationManager.removeEventListener(this);
			
			if(resResolver != null) resResolver.close();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onEvent(EventIterator events) {
		while(events.hasNext()) {
			Event currentEvent = events.nextEvent();
			try {
				logger.info("Event Occured at {}",currentEvent.getPath());
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
