package com.tekno.trainingexample.core.training;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;


@Component(service=Servlet.class,property= {
		"sling.servlet.resourceTypes=tekno/components/slingtest",
		"sling.servlet.selectors=print",
		"sling.servlet.methods=GET"
})
public class SlingServletRTEx extends SlingAllMethodsServlet {
		
	@Reference
	EventAdmin eventAdmin;
	
	public void doGet(SlingHttpServletRequest req, SlingHttpServletResponse response) throws IOException {
		response.getWriter().println("In Servlet");
		
		Map<String, Object> properties = new HashMap<String,Object>();
		properties.put("resourcepath", req.getResource().getPath());
		Event event = new Event("com/tekno/training/event/topic", properties);
		eventAdmin.sendEvent(event);
		
	}
	
}
